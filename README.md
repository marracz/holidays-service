# holidays-service
Service for fetching holidays information.

## Development

```
mvn clean package
docker build -t holidays-service target/
docker run -p 8080:8080 -e API_KEY=[PLACE API KEY HERE] -it holidays-service
```

After packaging application there is generated jacoco tests report in `target/site/jacoco/index.html` 

## API documentation

* `/api/v1/holidays/{countryCode1}/{countryCode2}/{date}` -  returns next holiday after the given date that will happen on the same day in both countries

Example:
```
curl -i -H "Content-Type: application/json" 'http://localhost:8080/api/v1/holidays/polish/german/2021-02-01'
HTTP/1.1 200 
Content-Type: application/json
Transfer-Encoding: chunked
Date: Fri, 08 Jan 2021 08:06:28 GMT

{"date":"2021-02-14","name1":"Walentynki","name2":"Valentinstag"}
```

## Assumptions
Application uses Google Calendar API for retrieving needed data about holidays.

This API supports country codes as for example "polish", "german", "spain", etc.
Additionally to specify language of country events, API requires shortcut "pl", "de", "es", etc.
Some of this country code are mapped to matching language shortcut in `application.yml`.
Others are printed in English.