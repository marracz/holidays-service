package com.bluestone.holidaysservice.holiday.ports;

import com.bluestone.holidaysservice.adapters.googlecalendar.model.CountryCalendar;

import java.time.LocalDate;

public interface CalendarClient {

    CountryCalendar getCountryEventsAfterDate(String calendarName, LocalDate date);
}
