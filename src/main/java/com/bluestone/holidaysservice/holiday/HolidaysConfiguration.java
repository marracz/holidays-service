package com.bluestone.holidaysservice.holiday;

import com.bluestone.holidaysservice.holiday.ports.CalendarClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class HolidaysConfiguration {

    @Bean
    public HolidaysService holidaysService(CalendarClient calendarClient) {
        return new HolidaysService(calendarClient);
    }
}
