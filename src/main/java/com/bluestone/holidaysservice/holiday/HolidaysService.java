package com.bluestone.holidaysservice.holiday;

import com.bluestone.holidaysservice.holiday.model.CommonHoliday;
import com.bluestone.holidaysservice.holiday.model.CommonHolidayNotFoundException;
import com.bluestone.holidaysservice.holiday.model.Holiday;
import com.bluestone.holidaysservice.holiday.model.HolidaysCalendar;
import com.bluestone.holidaysservice.holiday.ports.CalendarClient;
import com.google.common.collect.Sets;
import lombok.extern.slf4j.Slf4j;

import java.time.LocalDate;
import java.util.Set;
import java.util.stream.Collectors;

@Slf4j
public class HolidaysService {

    private final CalendarClient calendarClient;

    public HolidaysService(CalendarClient calendarClient) {
        this.calendarClient = calendarClient;
    }

    public CommonHoliday findCommonHolidayInCountriesAfterDate(String countryCode1,
                                                               String countryCode2,
                                                               LocalDate date) {
        HolidaysCalendar country1Calendar = calendarClient.getCountryEventsAfterDate(countryCode1, date);
        HolidaysCalendar country2Calendar = calendarClient.getCountryEventsAfterDate(countryCode2, date);


        String commonEventDate = findFirstCommonHolidayInCountries(country1Calendar, country2Calendar);

        String name1 = findCountryHolidayName(country1Calendar, commonEventDate);
        String name2 = findCountryHolidayName(country2Calendar, commonEventDate);

        return CommonHoliday.builder()
                .date(commonEventDate)
                .holidayName(name1)
                .correspondingHolidayName(name2)
                .build();
    }

    private String findFirstCommonHolidayInCountries(HolidaysCalendar country1Calendar, HolidaysCalendar country2Calendar) {
        return Sets.intersection(extractDates(country1Calendar.getHolidays()),
                                 extractDates(country2Calendar.getHolidays()))
                .stream()
                .min(String::compareTo)
                .orElseThrow(() -> new CommonHolidayNotFoundException("Not found common holiday in specified countries"));
    }

    private Set<String> extractDates(Set<? extends Holiday> holidays) {
        return holidays.stream()
                .map(Holiday::getDate)
                .collect(Collectors.toSet());
    }

    private String findCountryHolidayName(HolidaysCalendar holidaysCalendar, String commonEventDate) {
        return holidaysCalendar.getHolidays().stream()
                .filter(x -> commonEventDate.equals(x.getDate()))
                .findFirst()
                .map(Holiday::getName)
                .orElse(null);
    }

}
