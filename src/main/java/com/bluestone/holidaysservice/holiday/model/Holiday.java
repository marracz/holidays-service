package com.bluestone.holidaysservice.holiday.model;

public interface Holiday {

    String getName();

    String getDate();
}
