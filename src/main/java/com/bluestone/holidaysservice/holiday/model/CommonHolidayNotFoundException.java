package com.bluestone.holidaysservice.holiday.model;

public class CommonHolidayNotFoundException extends RuntimeException {

    public CommonHolidayNotFoundException(String message) {
        super(message);
    }
}
