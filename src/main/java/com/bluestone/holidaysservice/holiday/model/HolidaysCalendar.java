package com.bluestone.holidaysservice.holiday.model;

import java.util.Set;

public interface HolidaysCalendar {

    Set<? extends Holiday> getHolidays();
}
