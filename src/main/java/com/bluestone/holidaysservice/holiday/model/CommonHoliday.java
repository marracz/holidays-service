package com.bluestone.holidaysservice.holiday.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CommonHoliday {

    @JsonProperty("date")
    private String date;
    @JsonProperty("name1")
    private String holidayName;
    @JsonProperty("name2")
    private String correspondingHolidayName;
}
