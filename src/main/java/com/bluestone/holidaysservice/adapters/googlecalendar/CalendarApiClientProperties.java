package com.bluestone.holidaysservice.adapters.googlecalendar;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@ConfigurationProperties(prefix = "calendar.api")
public class CalendarApiClientProperties {

    private String urlTemplate;
}
