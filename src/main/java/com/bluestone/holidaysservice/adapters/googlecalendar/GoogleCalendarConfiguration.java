package com.bluestone.holidaysservice.adapters.googlecalendar;

import com.fasterxml.jackson.databind.ObjectMapper;
import okhttp3.OkHttpClient;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@EnableConfigurationProperties(value = {CalendarApiClientProperties.class, CountryCodesProperties.class})
@Configuration
public class GoogleCalendarConfiguration {

    @Bean
    public GoogleCalendarClient googleCalendarClient(OkHttpClient okHttpClient,
                                                     ObjectMapper objectMapper,
                                                     CalendarApiClientProperties calendarApiClientProperties,
                                                     CountryCodesProperties countryCodesProperties) {
        return new GoogleCalendarClient(okHttpClient, objectMapper, calendarApiClientProperties, countryCodesProperties);
    }
}
