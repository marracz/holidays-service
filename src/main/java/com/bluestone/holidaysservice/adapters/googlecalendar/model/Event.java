package com.bluestone.holidaysservice.adapters.googlecalendar.model;

import com.bluestone.holidaysservice.holiday.model.Holiday;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class Event implements Holiday {

    @JsonProperty("summary")
    private String name;
    @JsonProperty("start")
    private Start start;

    @Override
    public String getDate() {
        return start.getDate();
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    @Data
    public static class Start {
        @JsonProperty("date")
        private String date;
    }

}
