package com.bluestone.holidaysservice.adapters.googlecalendar.model;

public class CalendarNotFoundException extends RuntimeException {
    public CalendarNotFoundException(String message) {
        super(message);
    }
}
