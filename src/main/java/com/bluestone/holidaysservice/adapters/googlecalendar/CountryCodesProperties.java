package com.bluestone.holidaysservice.adapters.googlecalendar;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.Map;

@Data
@ConfigurationProperties(prefix = "country-codes")
public class CountryCodesProperties {

    private String defaultLanguage;
    private Map<String, String> mapping;
}
