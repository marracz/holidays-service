package com.bluestone.holidaysservice.adapters.googlecalendar;

import com.bluestone.holidaysservice.adapters.googlecalendar.model.CalendarNotFoundException;
import com.bluestone.holidaysservice.adapters.googlecalendar.model.CountryCalendar;
import com.bluestone.holidaysservice.holiday.ports.CalendarClient;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import java.io.IOException;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Objects;

@Slf4j
public class GoogleCalendarClient implements CalendarClient {

    private static final String TIME_MIN_PARAMETER = "timeMin";
    private static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ssXXX");

    private final OkHttpClient okHttpClient;
    private final ObjectMapper objectMapper;
    private final CalendarApiClientProperties calendarApiClientProperties;
    private final CountryCodesProperties countryCodesProperties;

    public GoogleCalendarClient(OkHttpClient okHttpClient,
                                ObjectMapper objectMapper,
                                CalendarApiClientProperties calendarApiClientProperties,
                                CountryCodesProperties countryCodesProperties) {
        this.okHttpClient = okHttpClient;
        this.objectMapper = objectMapper;
        this.calendarApiClientProperties = calendarApiClientProperties;
        this.countryCodesProperties = countryCodesProperties;
    }

    @Override
    public CountryCalendar getCountryEventsAfterDate(String calendarName, LocalDate date) {
        String isoDate = formatDate(date);

        String languageCode = countryCodesProperties.getMapping().getOrDefault(calendarName,
                countryCodesProperties.getDefaultLanguage());

        HttpUrl httpUrl = buildHttpUrl(calendarName, isoDate, languageCode);

        Request request = new Request.Builder()
                .url(httpUrl)
                .build();
        try {
            Response response = okHttpClient.newCall(request).execute();
            handleApiErrors(calendarName, response);
            return objectMapper.readValue(response.body().string(), CountryCalendar.class);
        } catch (IOException e) {
            log.error("API for fetching holidays data failed", e);
            throw new ApiException("API for fetching holidays data failed", e);
        }
    }

    private void handleApiErrors(String calendarName, Response response) {
        if (response.code() == 404) {
            throw new CalendarNotFoundException("Missing ".concat(calendarName).concat(" calendar"));
        }
        if (response.code() != 200) {
            throw new ApiException("API for fetching holidays data failed");
        }
    }

    private String formatDate(LocalDate date) {
        return DATE_TIME_FORMATTER.withZone(ZoneId.systemDefault())
                .format(date.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());
    }

    private HttpUrl buildHttpUrl(String calendarName, String isoDate, String languageCode) {
        return Objects.requireNonNull(HttpUrl.parse(String.format(calendarApiClientProperties.getUrlTemplate(), languageCode, calendarName)))
                .newBuilder()
                .addQueryParameter(TIME_MIN_PARAMETER, isoDate).build();
    }

    public static class ApiException extends RuntimeException {

        public ApiException(String message) {
            super(message);
        }

        public ApiException(String message, Throwable throwable) {
            super(message, throwable);
        }
    }

}
