package com.bluestone.holidaysservice.adapters.googlecalendar.model;

import com.bluestone.holidaysservice.holiday.model.Holiday;
import com.bluestone.holidaysservice.holiday.model.HolidaysCalendar;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.Set;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class CountryCalendar implements HolidaysCalendar {

    @JsonProperty("items")
    private Set<Event> events;

    @Override
    public Set<? extends Holiday> getHolidays() {
        return events;
    }
}
