package com.bluestone.holidaysservice.adapters.api;

import com.bluestone.holidaysservice.adapters.googlecalendar.GoogleCalendarClient;
import com.bluestone.holidaysservice.adapters.googlecalendar.model.CalendarNotFoundException;
import com.bluestone.holidaysservice.holiday.model.CommonHolidayNotFoundException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class ExceptionHandlerConfiguration extends ResponseEntityExceptionHandler {

    @ExceptionHandler(value
            = {CommonHolidayNotFoundException.class, CalendarNotFoundException.class})
    protected ResponseEntity<Object> handleNotFoundError(
            RuntimeException ex, WebRequest request) {
        return handleExceptionInternal(ex, ex.getMessage(),
                new HttpHeaders(), HttpStatus.NOT_FOUND, request);
    }

    @ExceptionHandler(value
            = {GoogleCalendarClient.ApiException.class})
    protected ResponseEntity<Object> handleInternalServerError(
            RuntimeException ex, WebRequest request) {
        return handleExceptionInternal(ex, ex.getMessage(),
                new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR, request);
    }

}
