package com.bluestone.holidaysservice.adapters.api;

import com.bluestone.holidaysservice.holiday.HolidaysService;
import com.bluestone.holidaysservice.holiday.model.CommonHoliday;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.time.LocalDate;

@RestController
public class HolidaysController {

    private final HolidaysService holidaysService;

    public HolidaysController(HolidaysService holidaysService) {
        this.holidaysService = holidaysService;
    }

    @GetMapping(value = "/api/v1/holidays/{countryCode1}/{countryCode2}/{date}", produces = MediaType.APPLICATION_JSON_VALUE)
    public CommonHoliday fetchCommonHolidayInCountriesAfterDate(@PathVariable("countryCode1") String countryCode1,
                                                                @PathVariable("countryCode2") String countryCode2,
                                                                @PathVariable @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate date) throws IOException {
        return holidaysService.findCommonHolidayInCountriesAfterDate(countryCode1, countryCode2, date);
    }
}
