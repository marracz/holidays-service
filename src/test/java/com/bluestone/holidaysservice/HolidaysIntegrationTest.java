package com.bluestone.holidaysservice;

import com.bluestone.holidaysservice.holiday.model.CommonHoliday;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.tomakehurst.wiremock.WireMockServer;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static com.bluestone.holidaysservice.TestHelper.fromFile;
import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.get;
import static com.github.tomakehurst.wiremock.client.WireMock.urlPathMatching;
import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@AutoConfigureMockMvc
@SpringBootTest(properties = "calendar.api.url-template: http://localhost:9999/calendar/%s/%s")
public class HolidaysIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    private static final WireMockServer CALENDAR_API_SERVER = new WireMockServer(9999);

    @BeforeAll
    static void setUp() {
        CALENDAR_API_SERVER.start();
    }

    @AfterAll
    static void afterAll() {
        CALENDAR_API_SERVER.stop();
    }

    @Test
    public void shouldReturnCommonHoliday() throws Exception {
        // given
        CALENDAR_API_SERVER.stubFor(get(urlPathMatching("/calendar/pl/polish"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json")
                        .withBody(fromFile("classpath:mock-calendar-api-response-pl-200.json"))));

        CALENDAR_API_SERVER.stubFor(get(urlPathMatching("/calendar/de/german"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json")
                        .withBody(fromFile("classpath:mock-calendar-api-response-de-200.json"))));

        // when
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("/api/v1/holidays/polish/german/2021-02-01"))
                .andExpect(status().isOk())
                .andReturn();

        // then
        CommonHoliday commonHoliday = objectMapper.readValue(mvcResult.getResponse().getContentAsString(), CommonHoliday.class);
        assertThat(commonHoliday).isEqualTo(CommonHoliday.builder()
                .date("2021-02-14")
                .holidayName("Walentynki")
                .correspondingHolidayName("Valentinstag")
                .build());
    }

    @Test
    public void shouldReturnNotFoundStatusCodeWhenNoCommonHolidayExists() throws Exception {
        // given
        CALENDAR_API_SERVER.stubFor(get(urlPathMatching("/calendar/pl/polish"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json")
                        .withBody(fromFile("classpath:mock-calendar-api-response-pl-200.json"))));

        CALENDAR_API_SERVER.stubFor(get(urlPathMatching("/calendar/de/german"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json")
                        .withBody(fromFile("classpath:mock-calendar-api-response-de-200-empty-common-set.json"))));

        // when
        mockMvc.perform(MockMvcRequestBuilders.get("/api/v1/holidays/polish/german/2021-02-01"))
                .andExpect(status().isNotFound())
                .andReturn();

    }

    @Test
    public void shouldReturnNotFoundStatusCodeWhenCalendarNotExists() throws Exception {
        // given
        CALENDAR_API_SERVER.stubFor(get(urlPathMatching("/calendar/pl/polish"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json")
                        .withBody(fromFile("classpath:mock-calendar-api-response-pl-200.json"))));

        CALENDAR_API_SERVER.stubFor(get(urlPathMatching("/calendar/de/german"))
                .willReturn(aResponse()
                        .withStatus(404)));

        // when
        mockMvc.perform(MockMvcRequestBuilders.get("/api/v1/holidays/polish/german/2021-02-01"))
                .andExpect(status().isNotFound())
                .andReturn();

    }

    @Test
    public void shouldReturnApiServerErrorWhenCalendarApiFailed() throws Exception {
        // given
        CALENDAR_API_SERVER.stubFor(get(urlPathMatching("/calendar/pl/polish"))
                .willReturn(aResponse()
                        .withStatus(500)));

        // when
        mockMvc.perform(MockMvcRequestBuilders.get("/api/v1/holidays/polish/german/2021-02-01"))
                .andExpect(status().isInternalServerError())
                .andReturn();

    }

}
